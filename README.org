#+TITLE CLT2017 Effizient arbeiten mit GNU/Emacs

* Effizient arbeiten mit GNU/Emacs

  Abstract:
  GNU/Emacs ist ein mächtiges Textbearbeitungstool mit einer sehr
  langen Geschichte. Die Einstiegshürde ist, ähnlich wie bei vi(m)
  relativ hoch, doch mit gezielt gewählten Erweiterungen lässt sich
  die Barriere deutlich senken.

  Ein wesentlicher Teil des Vortrags wird zudem ein Einführung in das
  "Getting things done"-Paket org-mode sein, mit dem man Aufgaben und
  Termine effizient managen kann. Außerdem werden weitere nützliche
  Erweiterungen zum effizienten Arbeiten mit Emacs vorgestellt.


** Short Paper

*** Motivation

    Bei der Arbeit am Rechner haben wir oft mit Text zu tun, lesen,
    schreiben, editieren.

    GNU/Emacs ist ein erweiterbarer und anpassbarer Texteditor mit
    über 30 jähriger Geschichte. Das Programm ist jedoch nicht nur zum
    Bearbeiten von Text geeignet, bietet durch das enorme
    Ökosystem von Plugins und Erweiterungen die Möglichkeit auch
    andere Aufgaben effizient zu erledigen. So zum Beispiel das
    Managen von Projekten, Aufgaben und TODO-Listen. Außerdem enthält
    es einen serh komfortablen Dateimanager und (für Entwickler
    interessant) hervorragende Unterstützung für Programmieraufgaben
    und Versionskontrollsysteme.

    Durch seine anfangs etwas gewöhnungsbedürftige Bedienweise und
    enorme Flut an Dokumentation kann der Einstieg in Emacs schwer
    fallen. Mit diesem Vortrag möchte ich einige Tricks zeigen um
    diese Hürde stark zu senken und darstellen, wie sich die Mühe
    auszahlen kann. 

*** Inhalte

    Nach einer kurzen Einleitung in die Geschichte und Philosophie von
    GNU/Emacs wird mit der Erweiterung `ergoemacs` gezeigt, wie sich
    gewohnte Tastenkombinationen einstellen lassen und grundlegende
    Navigation vereinfacht wird.
    
    Mit "org-mode" stelle ich das in GNU/Emacs integrierte Programm
    zum Verwalten von Notizen, Aufgaben und Projekten vor. Dabei
    werden einige Anwendungsfälle und Tricks sowie
    Beispielkonfigurationen gezeigt.

    Für wirklich flüssiges effizientes Arbeiten werden zu guter letzt
    Erweiterungen besonders für Entwickler vorgestellt:
    
    - Magit, das angenehmste Interface für Git
    - Projectile, ein effizientes Tool für das Arbeiten mit
      (Programmier-)Projekten 
    - Helm, ein Framework für inkrementelle Suche und
      Autovervollständigung 
    - auto-complete/company-mode, Hilfswerkzeuge zur
      Autovervollständigung von Texten und Code-Schnipseln
    - und vielleicht noch ein paar andere interessante Stückchen

    Auch die Vermittlung von etwas Nerdkultur und "religiöse"
    Anspielungen werden nicht zu kurz kommen ;-)

*** nötiges Vorwissen

- keine besonderen Vorkenntnisse nötig
- Motivation lieber Tastatur als Maus zu benutzen

*** Links

- GNU/Emacs: https://www.gnu.org/software/emacs/
- ergoemacs-mode: http://ergoemacs.github.io/
- Einführung zum org-mode: http://doc.norang.ca/org-mode.html
  - http://orgmode.org/worg/org-tutorials/orgtutorial_dto.html


** License

This work is licensed under Creative Commons
Attribution-NonCommercial-ShareAlike 4.0 International License. To
view a copy of this license, visit
http://creativecommons.org/licenses/by-nc-sa/4.0/ or send a letter to
Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
